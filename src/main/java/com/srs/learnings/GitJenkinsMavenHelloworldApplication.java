package com.srs.learnings;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class GitJenkinsMavenHelloworldApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(GitJenkinsMavenHelloworldApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		for (int i = 100; i < 115; i++) {
			System.out.println("Jenkins build for git based maven project ---> " + i);
		}
	}
	
	@GetMapping
	public String greet() {
		return "<h1>Welcome to Jenkins build deployed to CF...!</h1>";
	}
	
	@GetMapping("/welcome")
	public String greetings() {
		return "<h1>Successfully built maven project from GIT with Jenkins and deployed to CF...!</h1>";
	}
}
